package mooc.vandy.java4android.diamonds.logic;

import mooc.vandy.java4android.diamonds.ui.OutputInterface;

/**
 * This is where the logic of this App is centralized for this assignment.
 * <p/>
 * The assignments are designed this way to simplify your early
 * Android interactions.  Designing the assignments this way allows
 * you to first learn key 'Java' features without having to beforehand
 * learn the complexities of Android.
 */
public class Logic implements LogicInterface {
    /**
     * This is a String to be used in Logging (if/when you decide you
     * need it for debugging).
     */
    public static final String TAG = Logic.class.getName();

    /**
     * This is the variable that stores our OutputInterface instance.
     * <p/>
     * This is how we will interact with the User Interface [MainActivity.java].
     * <p/>
     * It is called 'out' because it is where we 'out-put' our
     * results. (It is also the 'in-put' from where we get values
     * from, but it only needs 1 name, and 'out' is good enough).
     */
    private OutputInterface mOut;

    /**
     * This is the constructor of this class.
     * <p/>
     * It assigns the passed in [MainActivity] instance (which
     * implements [OutputInterface]) to 'out'.
     */
    public Logic(OutputInterface out) {
        mOut = out;
    }

    /**
     * This is the method that will (eventually) get called when the
     * on-screen button labeled 'Process...' is pressed.
     */
    public void process(int size) {

        int extraLines = size - 1;

        // TODO -- add your code here
        upperBorder(size);
        // line - start
        for (int lineNumber = 1; lineNumber <= extraLines; lineNumber++) {
            beginLine();

            for (int spaces = extraLines; spaces >= lineNumber; spaces--) {
                print(" ");
            }
            print("/");
            innerLine(lineNumber);
            print("\\");
            for (int spaces = extraLines; spaces >= lineNumber; spaces--) {
                print(" ");
            }
            endLine();
        }

        beginLine();
        mainLine(size);
        endLine();

        for (int lineNumber = extraLines; lineNumber >= 1; lineNumber--) {
            beginLine();

            for (int spaces = extraLines; spaces >= lineNumber; spaces--) {
                print(" ");
            }
            print("\\");
            innerLine(lineNumber);
            print("/");
            for (int spaces = extraLines; spaces >= lineNumber; spaces--) {
                print(" ");
            }
            endLine();
        }
        // line - end
        upperBorder(size);
    }


    private void println() {
        mOut.println("");
    }

    private void print(String character) {
        mOut.print(character);
    }

    private void print(String character, int num) {
        for (int i = 0; i < num; i++) {
            mOut.print(character);
        }
    }

    private void innerLine(int lineNumber) {
        for (int i = 1; i < lineNumber; i++) {
            if (lineNumber % 2 == 0) {
                print("--");
            } else {
                print("==");
            }
        }
    }

    private void mainLine(int lineNumber) {
        print("<");
        innerLine(lineNumber);
        print(">");
    }

    private void upperBorder(int size) {
        print("+");
        print("--", size);
        print("+");
        println();
    }

    private void beginLine() {
        print("|");
    }

    private void endLine() {
        print("|");
        println();
    }
}
