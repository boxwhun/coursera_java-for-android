package mooc.vandy.java4android.gate.logic;

import java.util.Random;

import mooc.vandy.java4android.gate.ui.OutputInterface;

/**
 * This class uses your Gate class to manage a herd of snails.  We
 * have supplied you will the code necessary to execute as an app.
 * You must fill in the missing logic below.
 */
public class HerdManager {
    /**
     * Reference to the output.
     */
    private OutputInterface mOut;

    /**
     * The input Gate object.
     */
    private Gate mEastGate;

    /**
     * The output Gate object.
     */
    private Gate mWestGate;

    /**
     * Maximum number of iterations to run the simulation.
     */
    private static final int MAX_ITERATIONS = 10;

    public static final int HERD = 24;

    /**
     * Constructor initializes the fields.
     */
    public HerdManager(OutputInterface out, Gate westGate, Gate eastGate) {
        this.mOut = out;

        this.mWestGate = westGate;
        this.mWestGate.open(Gate.IN);

        this.mEastGate = eastGate;
        this.mEastGate.open(Gate.OUT);
    }

    public void simulateHerd(Random rand) {
        int _herd, in, out;

        for (int i = 0; i <= MAX_ITERATIONS; i++) {
            _herd = Math.abs(rand.nextInt() % HERD);

            in = mWestGate.thru(_herd);
            out = Math.abs(mEastGate.thru(HERD - _herd));
            this.mOut.println(String.format("There are currently %d snails in the pen and %d snails in the pasture",
                                            in, out));
        }

    }
}
